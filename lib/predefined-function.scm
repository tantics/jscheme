(define map
  (lambda (proc items)
    (if (null? items)
        nil
        (cons (proc (car items))
              (map proc (cdr items))))))

;(define reduce
;  (lambda (op initial sequence)
;    (if (null? sequence)
;        initial
;        (op (car sequence)
;            (reduce op initial (cdr sequence))))))
;
;(define filter
;  (lambda (predicate sequence)
;    (if (null? sequence)
;        nil
;        (if (predicate (car sequence))
;            (cons (car sequence)
;                  (filter predicate (cdr sequence)))
;            (filter predicate (cdr sequence))))))
;
;(define flatmap
;  (lambda (proc seq)
;    (reduce append nil (map proc seq))))
;
;(define append
;  (lambda (list1 list2)
;    (if (null? list1)
;        list2
;        (cons (car list1) (append (cdr list1) list2)))))
;
