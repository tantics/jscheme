package value;

/**
 * types of Lisp values
 *
 */

public interface Value {

    public enum ValueType {
        T_NIL, T_FALSE, T_TRUE, T_PAIR, T_SYMBOL, T_REAL, T_PROCEDURE, T_THUNK,
    }

    public ValueType getType();
}
