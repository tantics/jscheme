package value;

/**
 * thunk object used by trampoline
 *
 */
public class Thunk implements Value {
    public Thunk(Procedure func, Value[] args) {
        this.func = func;
        this.args = args;
    }

    public Procedure getFunc() {
        return func;
    }

    public Value[] getArgs() {
        return args;
    }

    @Override
    public ValueType getType() {
        return ValueType.T_THUNK;
    }

    @Override
    public String toString() {
        String res = "[THUNK " + func.toString() + " ";
        for (int i = 0; i < args.length; ++i)
            res += args[i].toString() + " ";
        return res + "]";
    }

    private Procedure func;
    private Value[] args;

}
