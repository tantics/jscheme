package value;

/**
 * mutable pair
 *
 */

public class Pair implements Value {

    public Pair(Value first, Value second) {
        this.first = first;
        this.second = second;
    }

    public Value car() {
        return first;
    }

    public Value cdr() {
        return second;
    }

    public void setCar(Value newVal) {
        first = newVal;
    }

    public void setCdr(Value newVal) {
        second = newVal;
    }

    @Override
    public ValueType getType() {
        return ValueType.T_PAIR;
    }

    @Override
    public String toString() {
        return "(" + first.toString() + " . " + second.toString() + ")";
    }

    private Value first;
    private Value second;
}
