package value;

import java.util.LinkedHashMap;

public class Symbol implements Value {

    private Symbol(String text) {
        this.text = text;
    }

    public static Symbol getInstance(String text) {
        if (symbolPool.containsKey(text))
            return symbolPool.get(text);
        Symbol sym = new Symbol(text);
        symbolPool.put(text, sym);
        return sym;
    }

    @Override
    public ValueType getType() {
        return ValueType.T_SYMBOL;
    }

    @Override
    public String toString() {
        if (text.equals("#t") || text.equals("#f"))
            return text;
        return text.toUpperCase();
    }

    private String text;

    // self-evaluating symbols
    public static final Symbol NIL = new Symbol("()");
    public static final Symbol FALSE = new Symbol("#f");
    public static final Symbol TRUE = new Symbol("#t");

    // symbol pool
    private static LinkedHashMap<String, Symbol> symbolPool = new LinkedHashMap<String, Symbol>();
}
