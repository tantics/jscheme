package value;

public abstract class Procedure implements Value {

    public Procedure() {
    }

    @Override
    public ValueType getType() {
        return ValueType.T_PROCEDURE;
    }

    public abstract Value run(Value[] args);

    // make a clone
    public abstract Procedure dup();

}
