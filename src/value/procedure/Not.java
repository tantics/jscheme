package value.procedure;

import value.Procedure;
import value.Symbol;
import value.Value;

public class Not extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[1];
        if (args[0] == Symbol.TRUE)
            return k.run(new Value[] { Symbol.FALSE });
        return k.run(new Value[] { Symbol.TRUE });
    }

    @Override
    public String toString() {
        return "Procedure not";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}