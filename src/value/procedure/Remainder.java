package value.procedure;

import value.Procedure;
import value.Real;
import value.Value;

public class Remainder extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[2];
        int a = (int) ((Real) args[0]).val;
        int b = (int) ((Real) args[1]).val;
        return k.run(new Value[] { new Real(a % b) });
    }

    @Override
    public String toString() {
        return "Procedure remainder";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
