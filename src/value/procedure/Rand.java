package value.procedure;

import java.util.Random;

import value.Procedure;
import value.Real;
import value.Value;

public class Rand extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[0];
        return k.run(new Value[] { new Real(new Random().nextDouble()) });
    }

    @Override
    public String toString() {
        return "Procedure rand";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
