package value.procedure;

import value.Procedure;
import value.Symbol;
import value.Value;

public class Display extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[1];
        System.out.print(args[0]);
        return k.run(new Value[] { Symbol.NIL });
    }

    @Override
    public String toString() {
        return "Procedure display";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
