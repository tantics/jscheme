package value.procedure;

import value.Procedure;
import value.Symbol;
import value.Value;

public class IsEqv extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[2];
        if (args[0] == args[1])
            return k.run(new Value[] { Symbol.TRUE });
        return k.run(new Value[] { Symbol.FALSE });
    }

    @Override
    public String toString() {
        return "Procedure eqv?";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
