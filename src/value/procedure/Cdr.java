package value.procedure;

import value.Pair;
import value.Procedure;
import value.Value;

public class Cdr extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[1];
        Pair p = (Pair) args[0];
        return k.run(new Value[] { p.cdr() });
    }

    @Override
    public String toString() {
        return "Procedure cdr";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
