package value.procedure;

import value.Pair;
import value.Procedure;
import value.Symbol;
import value.Value;

public class SetCdr extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[2];
        Pair p = (Pair) args[0];
        Value v = args[1];
        p.setCdr(v);
        return k.run(new Value[] { Symbol.NIL });
    }

    @Override
    public String toString() {
        return "Procedure set-cdr!";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
