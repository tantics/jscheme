package value.procedure;

import value.Pair;
import value.Procedure;
import value.Symbol;
import value.Value;

public class List extends Procedure {

    @Override
    public Value run(Value[] args) {
        int n = args.length;
        // the last argument is the continuation
        Procedure k = (Procedure) args[n - 1];
        Pair res = new Pair(args[n - 2], Symbol.NIL);
        for (int i = args.length - 3; i >= 0; --i)
            res = new Pair(args[i], res);
        return k.run(new Value[] { res });
    }

    @Override
    public String toString() {
        return "Procedure list";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
