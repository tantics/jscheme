package value.procedure;

import value.Procedure;
import value.Symbol;
import value.Value;

public class Newline extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[0];
        System.out.println();
        return k.run(new Value[] { Symbol.NIL });
    }

    @Override
    public String toString() {
        return "Procedure newline";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
