package value.procedure;

import value.Procedure;
import value.Real;
import value.Symbol;
import value.Value;

public class Equal extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[2];
        Real a = (Real) args[0];
        Real b = (Real) args[1];
        if (Math.abs(a.val - b.val) < EPSILON)
            return k.run(new Value[] { Symbol.TRUE });
        return k.run(new Value[] { Symbol.FALSE });
    }

    @Override
    public String toString() {
        return "Procedure =";
    }

    @Override
    public Procedure dup() {
        return this;
    }

    private static final double EPSILON = 1.0e-20;
}
