package value.procedure;

import value.Pair;
import value.Procedure;
import value.Value;

public class Cons extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[2];
        return k.run(new Value[] { new Pair(args[0], args[1]) });
    }

    @Override
    public String toString() {
        return "Procedure cons";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
