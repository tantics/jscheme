package value.procedure;

import value.Procedure;
import value.Real;
import value.Value;

public class Asin extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[1];
        double res = ((Real) args[0]).val;
        return k.run(new Value[] { new Real(Math.asin(res)) });
    }

    @Override
    public String toString() {
        return "Procedure asin";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
