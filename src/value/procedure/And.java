package value.procedure;

import value.Procedure;
import value.Symbol;
import value.Value;

public class And extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[args.length - 1];
        for (int i = 0; i < args.length - 1; ++i)
            if (args[i] != Symbol.TRUE)
                return k.run(new Value[] { Symbol.FALSE });
        return k.run(new Value[] { Symbol.TRUE });
    }

    @Override
    public String toString() {
        return "Procedure and";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
