package value.procedure;

import value.Procedure;
import value.Real;
import value.Value;

public class Abs extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[1];
        Real a = (Real) args[0];
        return k.run(new Value[] { new Real(Math.abs(a.val)) });
    }

    @Override
    public String toString() {
        return "Procedure abs";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
