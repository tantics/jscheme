package value.procedure;

import value.Procedure;
import value.Real;
import value.Value;

public class Multiply extends Procedure {

    @Override
    public Value run(Value[] args) {
        // the last argument is the continuation
        Procedure k = (Procedure) args[args.length - 1];
        double res = 1;
        for (int i = 0; i < args.length - 1; ++i) {
            res *= ((Real) args[i]).val;
        }
        return k.run(new Value[] { new Real(res) });
    }

    @Override
    public String toString() {
        return "Procedure *";
    }

    @Override
    public Procedure dup() {
        return this;
    }

}
