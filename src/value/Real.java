package value;

public class Real implements Value {

    public Real(double val) {
        this.val = val;
    }

    @Override
    public ValueType getType() {
        return ValueType.T_REAL;
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }

    public double val;

}
