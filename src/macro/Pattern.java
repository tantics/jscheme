package macro;

/*
 * macro pattern
 */

public class Pattern {

    public enum PatternType {
        T_NIL, T_LITERAL, T_NORMAL, T_UNDERSCORE, T_DOT, T_ELLIPSIS,
    }

    public Pattern(String text, PatternType type) {
        this.text = text;
        this.type = type;
    }

    public boolean isNil() {
        return this == NIL;
    }

    public PatternType getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public Pattern deepCopy() {
        return new Pattern(text, type);
    }

    @Override
    public String toString() {
        return "<" + type + " " + text + ">";
    }

    private PatternType type;
    private String text;

    public static final Pattern NIL = new Pattern("", PatternType.T_NIL);

}
