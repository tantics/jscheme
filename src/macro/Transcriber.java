package macro;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import macro.Pattern.PatternType;
import util.GenID;
import util.Num;
import core.Expr;

/**
 * macro expansion, which happens before compilation
 */

public class Transcriber {

    public static Expr transcribe(Expr exp) {
        macros.clear();
        macroTagTable.clear();
        collectMacros(exp);
        return do_trans(exp);
    }

    public static String getMacroTag(String name) {
        return macroTagTable.get(name);
    }

    // collect macro definitions and build macro tag table
    private static void collectMacros(Expr exp) {
        do_collect(exp, macros);
    }

    private static void do_collect(Expr exp, List<Expr> res) {
        if (exp.isNil() || exp.isAtom())
            return;

        // check exp itself
        Expr e1 = exp.getChild(0);
        if (e1.isAtom() && e1.getText().equals("define-syntax")) {
            res.add(exp);
            String tag = GenID.nextIdFor("__macro");
            macroTagTable.put(exp.getChild(1).getText(), tag);
            return;
        }

        do_collect(exp.first(), res);
        do_collect(exp.rest(), res);
    }

    // transcribe an s-expression
    public static Expr do_trans(Expr exp) {
        if (exp.isNil()
                || exp.isAtom()
                || (exp.isList() && exp.first().isAtom() && exp.first()
                        .getText().equals("define-syntax")))
            return exp;
        // make sure exp itself is not a macro application
        while (isMacro(exp))
            exp = do_macro(exp);
        if (exp.isList()) {
            Expr res = new Expr();
            for (int i = 0; i < exp.childrenCount(); ++i) {
                res.addChild(do_trans(exp.getChild(i)));
            }
            return res;
        }
        return exp;
    }

    private static boolean isMacro(Expr exp) {
        if (!exp.isList() || !exp.first().isAtom())
            return false;
        return isMacroName(exp.first().getText());
    }

    private static boolean isMacroName(String t) {
        for (Expr macro : macros) {
            String name = macro.getChild(1).getText();
            if (name.equals(t))
                return true;
        }
        return false;
    }

    // transcribe a list
    private static Expr do_macro(Expr exp) {
        for (Expr macro : macros) {
            // find matching macro
            String defName = macro.getChild(1).getText();
            String useName = exp.getChild(0).getText();
            if (!defName.equals(useName))
                continue;
            // find matching rule
            Expr rules = macro.getChild(2);
            Expr literals = rules.getChild(1);
            for (int i = 2; i < rules.childrenCount(); ++i) {
                Expr rule = rules.getChild(i);
                Expr template = rule.getChild(1);
                Queue<Pattern> pat = parsePattern(rule.getChild(0), literals);
                MatchResult match = matchPattern(pat, exp);
                if (match != null) {
                    String macroTag = macroTagTable.get(useName);
                    return do_replace(template, match, macroTag);
                }
            }
        }
        System.err.println("transcribe: no matching rule.");
        System.exit(1);
        return null;
    }

    private static Expr do_replace(Expr template, MatchResult match, String tag) {
        if (template.isNil())
            return new Expr();

        /* atom */
        if (template.isAtom()) {
            String t = template.getText();
            if (match.contains(t)) {
                Pattern pat = match.get(t);
                return match.get(pat);
            } else {
                if (keywords.contains(t) || Num.isNumber(t) || isMacroName(t))
                    return new Expr(t);
                return new Expr(tag + "$" + t);
            }
        }

        /* list */
        Expr res = new Expr();
        for (int i = 0; i < template.childrenCount(); ++i) {
            Expr child = template.getChild(i);
            // ellipsis pattern variable always refers to a list. flattern it.
            if (child.isAtom() && i < template.childrenCount() - 1) {
                Expr next = template.getChild(i + 1);
                if (next.isAtom() && next.getText().equals("...")) {
                    Expr res1 = do_replace(child, match, tag);
                    for (int j = 0; j < res1.childrenCount(); ++j)
                        res.addChild(res1.getChild(j));
                    ++i;
                    continue;
                }
            }
            res.addChild(do_replace(child, match, tag));
        }
        return res;
    }

    /**
     * match syntax pattern against s-expression
     */
    private static MatchResult matchPattern(Queue<Pattern> pat, Expr exp) {
        MatchResult res = new MatchResult();
        boolean matched = do_match(pat, exp, res);
        if (!matched)
            return null;
        return res;
    }

    private static boolean do_match(Queue<Pattern> pat, Expr exp,
            MatchResult res) {
        if (pat.isEmpty()) {
            if (exp.isNil())
                return true;
            return false;
        }

        Pattern pat1 = pat.poll();
        if (exp.isNil()) {
            if (pat1.getType() == PatternType.T_ELLIPSIS
                    || pat1.getType() == PatternType.T_NIL) {
                res.put(pat1, exp);
                return true;
            }
            return false;
        }

        Expr exp1 = exp.getChild(0);
        if (pat1.getType() == PatternType.T_NIL) {
            return false;
        } else if (pat1.getType() == PatternType.T_LITERAL) {
            if (exp1.isAtom() && exp1.getText().equals(pat1.getText())) {
                res.put(pat1, exp1);
                return do_match(pat, exp.rest(), res);
            }
            return false;
        } else if (pat1.getType() == PatternType.T_UNDERSCORE) {
            return do_match(pat, exp.rest(), res);
        } else if (pat1.getType() == PatternType.T_NORMAL) {
            res.put(pat1, exp1);
            return do_match(pat, exp.rest(), res);
        } else if (pat1.getType() == PatternType.T_DOT) {
            res.put(pat1, exp);
            return true;
        } else if (pat1.getType() == PatternType.T_ELLIPSIS) {
            res.put(pat1, exp);
            return true;
        } else {
            return true;
        }
    }

    /**
     * parse syntax pattern, result stored in a queue
     */
    private static Queue<Pattern> parsePattern(Expr pat, Expr literals) {
        LinkedList<Pattern> res = new LinkedList<Pattern>();
        do_parse(pat, literals, res);
        PatternType t = res.peekLast().getType();
        if (t != PatternType.T_DOT && t != PatternType.T_ELLIPSIS)
            res.add(Pattern.NIL);
        return res;
    }

    private static void do_parse(Expr pat, Expr literals, Queue<Pattern> res) {
        if (pat.isNil())
            return;

        /* atom */
        if (pat.isAtom()) {
            if (pat.getText().equals("_")) {
                res.offer(new Pattern(pat.getText(), PatternType.T_UNDERSCORE));
            } else if (isLiteral(pat, literals)) {
                res.offer(new Pattern(pat.getText(), PatternType.T_LITERAL));
            } else {
                res.offer(new Pattern(pat.getText(), PatternType.T_NORMAL));
            }
            return;
        }

        /* list */
        Expr first = pat.getChild(0);
        // special cases
        if (pat.childrenCount() > 1) {
            if (first.getText().equals(".")) {
                res.offer(new Pattern(pat.getChild(1).getText(),
                        PatternType.T_DOT));
                return;
            }
            Expr second = pat.getChild(1);
            if (second.getText().equals("...")) {
                res.offer(new Pattern(first.getText(), PatternType.T_ELLIPSIS));
                return;
            }
        }
        // normal cases
        do_parse(first, literals, res);
        do_parse(pat.rest(), literals, res);
    }

    private static boolean isLiteral(Expr exp, Expr literals) {
        if (literals == null || literals.isNil())
            return false;
        for (int i = 0; i < literals.childrenCount(); ++i) {
            if (literals.getChild(i).getText().equals(exp.getText())) {
                return true;
            }
        }
        return false;
    }

    // each macro definition has a unique macro tag
    private static List<Expr> macros = new LinkedList<Expr>();
    private static LinkedHashMap<String, String> macroTagTable = new LinkedHashMap<String, String>();
    private static LinkedHashSet<String> keywords = new LinkedHashSet<String>();
    static {
        String[] words = { "define", "if", "lambda", "set!", "quote", "#t",
                "#f", "nil" };
        for (String w : words)
            keywords.add(w);
    }
}

/**
 * match result of macro pattern and macro usage
 */

class MatchResult {

    public MatchResult() {
        bindings = new LinkedHashMap<Pattern, Expr>();
    }

    public int size() {
        return bindings.size();
    }

    public Expr get(Pattern key) {
        return bindings.get(key);
    }

    public Pattern get(String key) {
        for (Pattern p : bindings.keySet()) {
            if (p.getText().equals(key))
                return p;
        }
        return null;
    }

    public void put(Pattern key, Expr val) {
        bindings.put(key.deepCopy(), val.deepCopy());
    }

    public boolean contains(String key) {
        for (Pattern p : bindings.keySet()) {
            if (p.getText().equals(key))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return bindings.toString();
    }

    private LinkedHashMap<Pattern, Expr> bindings;

}
