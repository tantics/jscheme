package test;

import java.io.BufferedReader;
import java.io.FileReader;

import macro.Transcriber;
import core.CPS;
import core.Compiler;
import core.Expr;
import core.Parser;
import core.Reader;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader fexp = new BufferedReader(new FileReader(
                "examples/macro/swap.scm"));
        BufferedReader ffunc = new BufferedReader(new FileReader(
                "lib/predefined-function.scm"));
        BufferedReader fmacro = new BufferedReader(new FileReader(
                "lib/predefined-macro.scm"));
        Expr exp = Parser.parse(Reader.load(fexp, ffunc, fmacro));
        fexp.close();
        fmacro.close();

        System.out.println("Reader Expression =>\n  " + exp);
        Expr expanded = Transcriber.transcribe(exp);
        System.out.println("Macro Expansion =>\n  " + expanded);
        Expr cps = CPS.transform(expanded);
        System.out.println("CPS Transformation =>\n  " + cps);
        Compiler comp = new Compiler("bin/test/", "test", "Scheme");
        comp.compile(cps);
    }

}
