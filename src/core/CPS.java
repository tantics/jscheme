package core;

import macro.Transcriber;
import util.GenID;

/**
 * Continuation-Passing Style Transformation: transform s-expression to its tail
 * form.
 *
 */

public class CPS {

    public static Expr transform(Expr exp) {
        return cps(exp, IDENTITY);
    }

    /**
     * @param exp
     *            s-expression to be transformed
     * @param K
     *            saved continuation
     */
    private static Expr cps(Expr exp, Expr K) {
        /* nil and atom */
        if (exp.isNil() || exp.isAtom())
            return do_direct(exp, K);

        Expr e0 = exp.getChild(0);
        if (e0.isAtom()) {
            String t = e0.getText();
            if (t.equals("quote"))
                return do_direct(exp, K);
            if (t.equals("lambda"))
                return do_lambda(exp, K);
            if (t.equals("define"))
                return do_define(exp, K);
            if (t.equals("set!"))
                return do_assignment(exp, K);
            if (t.equals("if"))
                return do_if(exp, K);
            if (t.equals("begin"))
                return do_begin(exp, K);
        }

        /* function call */
        return do_funcall(exp, K);
    }

    /**
     * (CPS val K) = (K val)
     */
    private static Expr do_direct(Expr exp, Expr K) {
        Expr res = new Expr();
        res.addChild(K);
        res.addChild(exp);
        return res;
    }

    /**
     * (CPS (lambda (args) body) K) = (K (lambda (args, k) (defvars) (CPS body
     * k)))
     * 
     * NOTE: lambda keeps track of its defined variables. the compiler will use
     * this information to generate extra instructions to initialize these
     * defined variables. so the 'define' in lambda can be treated as 'set!'
     */
    private static Expr do_lambda(Expr exp, Expr K) {
        // collect defined variables and macro definitions
        Expr defvars = new Expr();
        for (int i = 2; i < exp.childrenCount(); ++i) {
            Expr ci = exp.getChild(i);
            if (ci.isList() && ci.first().isAtom()) {
                String t0 = ci.first().getText();
                if (t0.equals("define")) {
                    defvars.addChild(ci.getChild(1));
                } else if (t0.equals("define-syntax")) {
                    String tag = Transcriber.getMacroTag(ci.getChild(1)
                            .getText());
                    defvars.addChild(new Expr(tag));
                }
            }
        }

        Expr lbd = new Expr();
        Expr args = exp.getChild(1);
        Expr kvars = args.deepCopy();
        Expr kvar = new Expr(GenID.nextIdFor("__k"));
        kvars.addChild(kvar);
        lbd.addChild(new Expr("lambda"));
        lbd.addChild(kvars);
        lbd.addChild(defvars);

        // construct lambda body, the macro definitions are removed (since the
        // macro applications have been expanded and the macro tag information
        // is saved to defvars)
        Expr body = new Expr();
        body.addChild(new Expr("begin"));
        int nbody = exp.childrenCount() - 2;
        for (int i = 0; i < nbody; ++i) {
            Expr ci = exp.getChild(i + 2);
            if (ci.isList() && ci.first().isAtom()
                    && ci.first().getText().equals("define-syntax"))
                continue;
            body.addChild(exp.getChild(i + 2));
        }
        lbd.addChild(cps(body, kvar));

        Expr res = new Expr();
        res.addChild(K);
        res.addChild(lbd);
        return res;
    }

    /**
     * (CPS (begin first rest) K) = (CPS first (lambda (_) (CPS rest K)))
     */
    private static Expr do_begin(Expr exp, Expr K) {
        int nexp = exp.childrenCount() - 1;
        if (nexp == 1)
            return cps(exp.getChild(1), K);

        Expr kvars = new Expr();
        Expr kvar = new Expr(GenID.nextIdFor("__var"));
        kvars.addChild(kvar);

        Expr first = exp.getChild(1);
        Expr kexp = new Expr();
        kexp.addChild(new Expr("begin"));
        for (int i = 2; i < exp.childrenCount(); ++i)
            kexp.addChild(exp.getChild(i));
        Expr kbody = cps(kexp, K);

        Expr k = new Expr(); // continuation
        k.addChild(new Expr("lambda"));
        k.addChild(kvars);
        k.addChild(new Expr()); // defined variables
        k.addChild(kbody);

        return cps(first, k);
    }

    /**
     * see 'do_lambda' for why 'define' is transformed to 'set!'
     */
    private static Expr do_define(Expr exp, Expr K) {
        return do_assignment(exp, K);
    }

    /**
     * (CPS (set! var valexp) K) = (CPS valexp (lambda (val) (K (set! var
     * val))))
     */
    private static Expr do_assignment(Expr exp, Expr K) {
        Expr var = exp.getChild(1);
        Expr valexp = exp.getChild(2);

        Expr k = new Expr(); // continuation
        Expr kvars = new Expr();
        Expr kvar = new Expr(GenID.nextIdFor("__var"));
        kvars.addChild(kvar);

        Expr kexp1 = new Expr();
        kexp1.addChild(new Expr("set!"));
        kexp1.addChild(var);
        kexp1.addChild(kvar);

        Expr kbody = new Expr();
        kbody.addChild(K);
        kbody.addChild(kexp1);

        k.addChild(new Expr("lambda"));
        k.addChild(kvars);
        k.addChild(new Expr()); // defined variables
        k.addChild(kbody);

        return cps(valexp, k);
    }

    /**
     * (CPS (if pred conseq alt) K) = (CPS pred (lambda (val) (if val (CPS
     * conseq K) (CPS alt K))))
     */
    private static Expr do_if(Expr exp, Expr K) {
        Expr pred = exp.getChild(1);
        Expr conseq = exp.getChild(2);
        Expr alt = exp.getChild(3);

        Expr k = new Expr(); // continuation
        Expr kvars = new Expr();
        Expr kvar = new Expr(GenID.nextIdFor("__var"));
        kvars.addChild(kvar);

        Expr kbody = new Expr();
        kbody.addChild(new Expr("if"));
        kbody.addChild(kvar);
        kbody.addChild(cps(conseq, K));
        kbody.addChild(cps(alt, K));

        k.addChild(new Expr("lambda"));
        k.addChild(kvars);
        k.addChild(new Expr()); // defined variables
        k.addChild(kbody);
        return cps(pred, k);
    }

    /**
     * Expr containing no function calls is simple; otherwise, it is complex
     */
    private static boolean isSimple(Expr exp) {
        return exp.isNil() || exp.isAtom();
    }

    private static int firstComplexIndex(Expr exp) {
        for (int i = 0; i < exp.childrenCount(); ++i)
            if (!isSimple(exp.getChild(i)))
                return i;
        return -1;
    }

    /**
     * when all the sub expressions are simple: (CPS (simp0 simp1 ... simpn) K)
     * = (simp0 simp1 ... simpn K). otherwise, (CPS (simp0 simp1 exp2 exp3 ...
     * expn) K) = (CPS exp2 (lambda (var2) (CPS simp0 simp1 var2 exp3 ... expn)
     * K))
     */
    private static Expr do_funcall(Expr exp, Expr K) {
        int ci = firstComplexIndex(exp);
        if (ci < 0) {
            Expr res = exp.deepCopy();
            res.addChild(K);
            return res;
        }
        Expr complex = exp.getChild(ci);
        Expr k = new Expr(); // continuation
        Expr kvars = new Expr();
        Expr kvar = new Expr(GenID.nextIdFor("__var"));
        kvars.addChild(kvar);

        Expr kexp = exp.deepCopy();
        kexp.setChild(ci, kvar);
        Expr kbody = cps(kexp, K);

        k.addChild(new Expr("lambda"));
        k.addChild(kvars);
        k.addChild(new Expr()); // defined variables
        k.addChild(kbody);
        return cps(complex, k);
    }

    /**
     * some special continuations
     */
    private static final Expr IDENTITY = Parser
            .parse("(lambda (__echo) () __echo)");

}
