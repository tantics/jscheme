package core;

import java.util.LinkedHashMap;

import value.Value;

/**
 * evaluation environment.
 *
 */

public class Env {
    public Env(Env outer) {
        this.outer = outer;
        this.bindings = new LinkedHashMap<String, Value>();
    }

    /**
     * @param key
     *            the target key
     * @Returns the binding for 'key' if it exists, exit otherwise.
     */
    public Value get(String key) {
        // special environment lookup rule for macro variables
        if (key.startsWith("__macro")) {
            Value val = try1(key);
            if (val != null) {
                // in this case, the name is introduced by a definition
                // genetated by macro expansion
                return val;
            }
            int i = key.indexOf("$");
            String macro = key.substring(0, i);
            Env env = find(macro);
            if (env == null) {
                System.err.println("Env: environment not found for '" + macro
                        + "'");
                System.exit(1);
            }
            String var = key.substring(i + 1);
            return env.get(var);
        }
        return get1(key);
    }

    private Value get1(String key) {
        Value res = try1(key);
        if (res == null) {
            System.err.println("Env: undefined variable '" + key + "'");
            System.exit(1);
        }
        return res;
    }

    private Value try1(String key) {
        if (bindings.containsKey(key))
            return bindings.get(key);
        if (outer != null)
            return outer.try1(key);
        return null;
    }

    /**
     * create new binding
     */
    public void put(String key, Value val) {
        bindings.put(key, val);
    }

    /**
     * update binding
     */
    public void update(String key, Value val) {
        if (bindings.containsKey(key) || outer == null) {
            bindings.put(key, val);
        } else {
            outer.update(key, val);
        }
    }

    /**
     * @param key
     * @return neatest environment containing key; null if not found.
     */
    public Env find(String key) {
        if (bindings.containsKey(key))
            return this;
        if (outer != null)
            return outer.find(key);
        return null;
    }

    private Env outer;
    private LinkedHashMap<String, Value> bindings;
}
