package core;

import java.io.BufferedReader;
import java.io.IOException;

public class Reader {

    /**
     * load predefined macros, read all s-expressions from a stream, pack them
     * into a lambda expression
     */
    public static String load(BufferedReader fexp, BufferedReader ffunc,
            BufferedReader fmacro) {
        StringBuilder res = new StringBuilder();
        res.append("((lambda () ");
        do_load(ffunc, res);
        do_load(fmacro, res);
        do_load(fexp, res);
        res.append(" ))");
        return res.toString();
    }

    private static void do_load(BufferedReader fexp, StringBuilder res) {
        while (true) {
            String exp = read1(fexp);
            if (exp == null)
                break;
            res.append(exp + " ");
        }
    }

    /**
     * read an s-expression from a file input stream. an s-expression is either
     * an atom or a list of s-expressions enclosed by '()'.
     * 
     * @Returns return null if something goes wrong; otherwise, return the
     *          s-expression as a string.
     */

    public static String read1(BufferedReader fin) {
        StringBuilder sexp = new StringBuilder();
        try {
            // skip leading white spaces
            int c = fin.read();
            while (true) {
                if (c == -1)
                    return null;
                else if (c == ';')
                    fin.readLine();
                else if (!Character.isWhitespace(c))
                    break;
                c = fin.read();
            }
            // list
            if (c == '(') {
                int mismatch = 0;
                while (c != -1) {
                    if (c == ';')
                        fin.readLine();
                    else {
                        if (c == '(')
                            ++mismatch;
                        if (c == ')')
                            --mismatch;
                        sexp.append((char) c);
                    }
                    if (mismatch == 0)
                        break;
                    c = fin.read();
                }
                if (mismatch == 0)
                    return sexp.toString();
                return null;
            }
            // atom
            while (c != -1) {
                if (c == ';')
                    fin.readLine();
                else if (Character.isWhitespace(c))
                    break;
                else
                    sexp.append((char) c);
                c = fin.read();
            }
            return sexp.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null; // should never reach here
    }

}
