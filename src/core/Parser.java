package core;

import java.util.LinkedList;

/**
 * parse the s-expression string and build AST
 *
 */

public class Parser {
    public static Expr parse(String sexp) {
        LinkedList<String> exps = new LinkedList<String>();
        String ss[] = sexp.replaceAll("\\(", " ( ").replaceAll("\\)", " ) ")
                .split("\\s");
        for (String s : ss) {
            if (!s.isEmpty())
                exps.add(s);
        }
        return do_parse(exps);
    }

    private static Expr do_parse(LinkedList<String> exps) {
        if (exps.isEmpty())
            return null;
        String head = exps.pop();
        if (head.equals("(")) {
            Expr ast = new Expr();
            while (!exps.isEmpty()) {
                String e = exps.peek();
                if (e.equals(")")) {
                    exps.pop();
                    return ast;
                }
                ast.addChild(do_parse(exps));
            }
            return null; // should never reach here
        }
        return new Expr(head);
    }
}
