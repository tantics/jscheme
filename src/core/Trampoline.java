package core;

import value.Thunk;
import value.Value;
import value.Value.ValueType;

/**
 * trampoline is a technique to do tail call optimization.
 *
 */

public class Trampoline {

    public static Value exec(Value v) {
        while (v.getType() == ValueType.T_THUNK) {
            Thunk t = (Thunk) v;
            v = t.getFunc().run(t.getArgs());
        }
        return v;
    }

}