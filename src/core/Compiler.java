package core;

import java.io.FileOutputStream;
import java.util.LinkedHashMap;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import util.GenID;
import util.Num;

/**
 * compile s-expression to JVM byte code
 *
 */

public class Compiler implements Opcodes {

    /**
     * @param outputPath
     *            where to put the generated class file
     */
    public Compiler(String outputPath, String packageName, String className) {
        this.outputPath = outputPath;
        this.packageName = packageName;
        this.className = className;
    }

    public void compile(Expr exp) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES
                | ClassWriter.COMPUTE_MAXS);
        String fullName = packageName + "/" + className;
        cw.visit(V1_5, ACC_PUBLIC + ACC_SUPER, fullName, null,
                "java/lang/Object", null);

        /* field */
        FieldVisitor fv = cw.visitField(ACC_PRIVATE, "env", "Lcore/Env;", null,
                null);
        fv.visitEnd();

        /* constructor */
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null,
                null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V",
                false);
        // make global environment
        mv.visitVarInsn(ALOAD, 0);
        mv.visitTypeInsn(NEW, "core/Env");
        mv.visitInsn(DUP);
        mv.visitInsn(ACONST_NULL);
        mv.visitMethodInsn(INVOKESPECIAL, "core/Env", "<init>",
                "(Lcore/Env;)V", false);
        mv.visitFieldInsn(PUTFIELD, fullName, "env", "Lcore/Env;");
        // bind primitive functions
        LinkedHashMap<String, String> primFuncs = new LinkedHashMap<String, String>();
        primFuncs.put("+", "value/procedure/Add");
        primFuncs.put("-", "value/procedure/Subtract");
        primFuncs.put("*", "value/procedure/Multiply");
        primFuncs.put("/", "value/procedure/Divide");
        primFuncs.put(">", "value/procedure/GreaterThan");
        primFuncs.put("<", "value/procedure/LessThan");
        primFuncs.put("=", "value/procedure/Equal");
        primFuncs.put(">=", "value/procedure/GreaterEqual");
        primFuncs.put("<=", "value/procedure/LessEqual");
        primFuncs.put("abs", "value/procedure/Abs");
        primFuncs.put("and", "value/procedure/And");
        primFuncs.put("or", "value/procedure/Or");
        primFuncs.put("not", "value/procedure/Not");
        primFuncs.put("remainder", "value/procedure/Remainder");
        primFuncs.put("sin", "value/procedure/Sin");
        primFuncs.put("cos", "value/procedure/Cos");
        primFuncs.put("tan", "value/procedure/Tan");
        primFuncs.put("asin", "value/procedure/Asin");
        primFuncs.put("acos", "value/procedure/Acos");
        primFuncs.put("atan", "value/procedure/Atan");
        primFuncs.put("cons", "value/procedure/Cons");
        primFuncs.put("car", "value/procedure/Car");
        primFuncs.put("cdr", "value/procedure/Cdr");
        primFuncs.put("list", "value/procedure/List");
        primFuncs.put("null?", "value/procedure/IsNull");
        primFuncs.put("eqv?", "value/procedure/IsEqv");
        primFuncs.put("rand", "value/procedure/Rand");
        primFuncs.put("set-car!", "value/procedure/SetCar");
        primFuncs.put("set-cdr!", "value/procedure/SetCdr");
        primFuncs.put("display", "value/procedure/Display");
        primFuncs.put("newline", "value/procedure/Newline");
        for (String sym : primFuncs.keySet()) {
            String proc = primFuncs.get(sym);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
            mv.visitLdcInsn(sym);
            mv.visitTypeInsn(NEW, proc);
            mv.visitInsn(DUP);
            mv.visitMethodInsn(INVOKESPECIAL, proc, "<init>", "()V", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "core/Env", "put",
                    "(Ljava/lang/String;Lvalue/Value;)V", false);
        }
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        /* Scheme entry function */
        mv = cw.visitMethod(ACC_PUBLIC, "schemeEntry", "()V", null, null);
        mv.visitCode();
        do_compile(exp, mv, fullName);

        /* trampline */
        mv.visitMethodInsn(INVOKESTATIC, "core/Trampoline", "exec",
                "(Lvalue/Value;)Lvalue/Value;", false);

        // print the result
        mv.visitVarInsn(ASTORE, 1);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out",
                "Ljava/io/PrintStream;");
        mv.visitVarInsn(ALOAD, 1);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println",
                "(Ljava/lang/Object;)V", false);
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        /* main */
        mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main",
                "([Ljava/lang/String;)V", null, null);
        mv.visitCode();
        mv.visitTypeInsn(NEW, fullName);
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, fullName, "<init>", "()V", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, fullName, "schemeEntry", "()V", false);
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        dump(cw, outputPath, className);
    }

    /**
     * after calling do_compile, the result of 'exp' is put on the stack. in our
     * implementation, each expression has one and only one result.
     */
    private void do_compile(Expr exp, MethodVisitor mv, String fullName) {
        /* nil */
        if (exp.isNil()) {
            do_nil(mv);
            return;
        }

        /* atom */
        if (exp.isAtom()) {
            if (Num.isNumber(exp.getText())) {
                do_num(exp, mv);
            } else if (exp.getText().equals("#t")) {
                do_true(mv);
            } else if (exp.getText().equals("#f")) {
                do_false(mv);
            } else {
                do_sym(exp, mv, fullName);
            }
            return;
        }

        Expr e0 = exp.getChild(0);

        /* quote */
        if (e0.isAtom() && e0.getText().equals("quote")) {
            do_quote(exp.getChild(1), mv, fullName);
            return;
        }

        /* set! */
        if (e0.isAtom() && e0.getText().equals("set!")) {
            do_assignment(exp, mv, fullName);
            return;
        }

        /* lambda */
        if (e0.isAtom() && e0.getText().equals("lambda")) {
            String simpleProcName = GenID.nextIdFor("__proc");
            String procName = packageName + "/" + simpleProcName;
            do_lambda(exp, simpleProcName);
            // capture current environment
            mv.visitTypeInsn(NEW, procName);
            mv.visitInsn(DUP);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
            mv.visitMethodInsn(INVOKESPECIAL, procName, "<init>",
                    "(Lcore/Env;)V", false);
            return;
        }

        /* if */
        if (e0.isAtom() && e0.getText().equals("if")) {
            do_if(exp, mv, fullName);
            return;
        }

        /* function call */
        do_funcall(exp, mv, fullName);
    }

    private void do_quote(Expr quoted, MethodVisitor mv, String fullName) {
        if (quoted.isNil()) {
            do_nil(mv);
        } else if (quoted.isAtom()) {
            if (Num.isNumber(quoted.getText())) {
                do_num(quoted, mv);
            } else if (quoted.getText().equals("#t")) {
                do_true(mv);
            } else if (quoted.getText().equals("#f")) {
                do_false(mv);
            } else {
                do_quotesym(quoted, mv);
            }
        } else {
            do_quotelist(quoted, mv, fullName);
        }
    }

    private void do_quotesym(Expr exp, MethodVisitor mv) {
        mv.visitLdcInsn(exp.getText());
        mv.visitMethodInsn(INVOKESTATIC, "value/Symbol", "getInstance",
                "(Ljava/lang/String;)Lvalue/Symbol;", false);
    }

    private void do_quotelist(Expr quoted, MethodVisitor mv, String fullName) {
        mv.visitTypeInsn(NEW, "value/Pair");
        mv.visitInsn(DUP);
        Expr first = quoted.getChild(0);
        do_quote(first, mv, fullName);
        int nrest = quoted.childrenCount() - 1;
        if (nrest == 0) {
            do_nil(mv);
        } else {
            Expr rest = new Expr();
            for (int i = 1; i < quoted.childrenCount(); ++i)
                rest.addChild(quoted.getChild(i));
            do_quote(rest, mv, fullName);
        }
        mv.visitMethodInsn(INVOKESPECIAL, "value/Pair", "<init>",
                "(Lvalue/Value;Lvalue/Value;)V", false);
    }

    private void do_if(Expr exp, MethodVisitor mv, String fullName) {
        Expr pred = exp.getChild(1);
        Expr conseq = exp.getChild(2);
        Expr alt = exp.getChild(3);
        do_compile(pred, mv, fullName);
        mv.visitFieldInsn(GETSTATIC, "value/Symbol", "FALSE", "Lvalue/Symbol;");
        Label altLabel = new Label();
        Label endLabel = new Label();
        mv.visitJumpInsn(IF_ACMPEQ, altLabel);
        // conseq
        do_compile(conseq, mv, fullName);
        mv.visitJumpInsn(GOTO, endLabel);
        // alt
        mv.visitLabel(altLabel);
        do_compile(alt, mv, fullName);
        // end
        mv.visitLabel(endLabel);
    }

    private void do_nil(MethodVisitor mv) {
        mv.visitFieldInsn(GETSTATIC, "value/Symbol", "NIL", "Lvalue/Symbol;");
    }

    private void do_true(MethodVisitor mv) {
        mv.visitFieldInsn(GETSTATIC, "value/Symbol", "TRUE", "Lvalue/Symbol;");
    }

    private void do_false(MethodVisitor mv) {
        mv.visitFieldInsn(GETSTATIC, "value/Symbol", "FALSE", "Lvalue/Symbol;");
    }

    private void do_num(Expr exp, MethodVisitor mv) {
        double val = Double.parseDouble(exp.getText());
        mv.visitTypeInsn(NEW, "value/Real");
        mv.visitInsn(DUP);
        mv.visitLdcInsn(val);
        mv.visitMethodInsn(INVOKESPECIAL, "value/Real", "<init>", "(D)V", false);
    }

    /**
     * symbol lookup in evaluation environment
     */
    private void do_sym(Expr exp, MethodVisitor mv, String fullName) {
        String var = exp.getText();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
        mv.visitLdcInsn(var);
        mv.visitMethodInsn(INVOKEVIRTUAL, "core/Env", "get",
                "(Ljava/lang/String;)Lvalue/Value;", false);
    }

    private void do_assignment(Expr exp, MethodVisitor mv, String fullName) {
        String var = exp.getChild(1).getText();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
        mv.visitLdcInsn(var);
        Expr val = exp.getChild(2);
        do_compile(val, mv, fullName);
        mv.visitMethodInsn(INVOKEVIRTUAL, "core/Env", "update",
                "(Ljava/lang/String;Lvalue/Value;)V", false);
        // return value of the define expression is NIL here
        mv.visitFieldInsn(GETSTATIC, "value/Symbol", "NIL", "Lvalue/Symbol;");
    }

    /**
     * compiling a lambda expression will create a new class implementing
     * value.Procedure. a lambda is of form (lambda (args) (defvars) body).
     *
     * @param exp
     *            s-expression to be compiled
     * @param procName
     *            simple procedure name
     */
    private void do_lambda(Expr exp, String procName) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES
                | ClassWriter.COMPUTE_MAXS);
        String fullName = packageName + "/" + procName;
        cw.visit(V1_5, ACC_PUBLIC + ACC_SUPER, fullName, null,
                "value/Procedure", null);

        /* field */
        FieldVisitor fv = cw.visitField(ACC_PRIVATE, "env", "Lcore/Env;", null,
                null);
        fv.visitEnd();

        /* constructor */
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>",
                "(Lcore/Env;)V", null, null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESPECIAL, "value/Procedure", "<init>", "()V",
                false);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitTypeInsn(NEW, "core/Env");
        mv.visitInsn(DUP);
        mv.visitVarInsn(ALOAD, 1);
        mv.visitMethodInsn(INVOKESPECIAL, "core/Env", "<init>",
                "(Lcore/Env;)V", false);
        mv.visitFieldInsn(PUTFIELD, fullName, "env", "Lcore/Env;");
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        /* getType */
        mv = cw.visitMethod(ACC_PUBLIC, "getType", "()Lvalue/Value$ValueType;",
                null, null);
        mv.visitCode();
        mv.visitFieldInsn(GETSTATIC, "value/Value$ValueType", "T_PROCEDURE",
                "Lvalue/Value$ValueType;");
        mv.visitInsn(ARETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        /* duplicate */
        mv = cw.visitMethod(ACC_PUBLIC, "dup", "()Lvalue/Procedure;", null,
                null);
        mv.visitCode();
        mv.visitTypeInsn(NEW, fullName);
        mv.visitInsn(DUP);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
        mv.visitMethodInsn(INVOKESPECIAL, fullName, "<init>", "(Lcore/Env;)V",
                false);
        mv.visitInsn(ARETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        /* run */
        mv = cw.visitMethod(ACC_PUBLIC, "run", "([Lvalue/Value;)Lvalue/Value;",
                null, null);
        mv.visitCode();

        // bind parameters
        Expr formalArgs = exp.getChild(1);
        for (int i = 0; i < formalArgs.childrenCount(); ++i) {
            String farg = formalArgs.getChild(i).getText();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
            mv.visitLdcInsn(farg);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitLdcInsn(i);
            mv.visitInsn(AALOAD);
            mv.visitMethodInsn(INVOKEVIRTUAL, "core/Env", "put",
                    "(Ljava/lang/String;Lvalue/Value;)V", false);
        }

        // initialize defined variables
        Expr defVars = exp.getChild(2);
        for (int i = 0; i < defVars.childrenCount(); ++i) {
            String var = defVars.getChild(i).getText();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, fullName, "env", "Lcore/Env;");
            mv.visitLdcInsn(var);
            mv.visitFieldInsn(GETSTATIC, "value/Symbol", "NIL",
                    "Lvalue/Symbol;");
            mv.visitMethodInsn(INVOKEVIRTUAL, "core/Env", "put",
                    "(Ljava/lang/String;Lvalue/Value;)V", false);
        }

        // evaluate the function body
        do_compile(exp.getChild(3), mv, fullName);

        mv.visitInsn(ARETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        dump(cw, outputPath, procName);
    }

    /**
     * function call
     */
    private void do_funcall(Expr exp, MethodVisitor mv, String fullName) {
        int nc = exp.childrenCount();
        // evaluate the sub-expressions from right to left
        for (int i = nc - 1; i >= 0; --i) {
            Expr child = exp.getChild(i);
            do_compile(child, mv, fullName);
        }
        mv.visitTypeInsn(CHECKCAST, "value/Procedure");
        // use its clone to do actual work
        mv.visitMethodInsn(INVOKEVIRTUAL, "value/Procedure", "dup",
                "()Lvalue/Procedure;", false);
        for (int i = 0; i < nc; ++i) {
            mv.visitVarInsn(ASTORE, i + 1);
        }
        // pack arguments into an array
        mv.visitLdcInsn(nc - 1);
        mv.visitTypeInsn(ANEWARRAY, "value/Value");
        mv.visitVarInsn(ASTORE, nc + 1);
        for (int i = 0; i < nc - 1; ++i) {
            mv.visitVarInsn(ALOAD, nc + 1);
            mv.visitLdcInsn(i);
            mv.visitVarInsn(ALOAD, i + 2);
            mv.visitInsn(AASTORE);
        }

        // do not call it; make a thunk instead
        mv.visitTypeInsn(NEW, "value/Thunk");
        mv.visitInsn(DUP);
        mv.visitVarInsn(ALOAD, 1);
        mv.visitVarInsn(ALOAD, nc + 1);
        mv.visitMethodInsn(INVOKESPECIAL, "value/Thunk", "<init>",
                "(Lvalue/Procedure;[Lvalue/Value;)V", false);
    }

    /**
     * dump JVM byte code
     */
    private void dump(ClassWriter cw, String outpath, String name) {
        try {
            FileOutputStream fos = new FileOutputStream(outpath + name
                    + ".class");
            fos.write(cw.toByteArray());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String outputPath;
    private String packageName;
    private String className;

}
