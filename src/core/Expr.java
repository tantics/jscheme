package core;

import java.util.ArrayList;

/**
 * an implementation of Lisp s-expression
 *
 */

public class Expr {
    public Expr() {
        this.text = null;
        children = new ArrayList<Expr>();
    }

    public Expr(String text) {
        this.text = text;
        children = new ArrayList<Expr>();
    }

    public boolean isNil() {
        return text == null && children.isEmpty() || isAtom()
                && text.equals("nil");
    }

    public boolean isAtom() {
        return text != null;
    }

    public boolean isList() {
        return text == null && !children.isEmpty();
    }

    public Expr first() {
        return getChild(0);
    }

    public Expr rest() {
        Expr res = new Expr();
        for (int i = 1; i < childrenCount(); ++i) {
            res.addChild(getChild(i));
        }
        return res;
    }

    public int childrenCount() {
        return children.size();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void addChild(Expr child) {
        children.add(child.deepCopy());
    }

    public Expr getChild(int index) {
        return children.get(index);
    }

    public void setChild(int index, Expr child) {
        children.set(index, child);
    }

    @Override
    public String toString() {
        if (text == null && children.isEmpty())
            return "()";

        if (text != null)
            return text;

        StringBuilder msg = new StringBuilder();
        msg.append("(");
        for (int i = 0; i < children.size(); ++i) {
            msg.append(getChild(i).toString());
            if (i != children.size() - 1)
                msg.append(" ");
        }
        msg.append(")");
        return msg.toString();
    }

    public Expr deepCopy() {
        Expr res = new Expr(text);
        for (int i = 0; i < childrenCount(); ++i) {
            res.addChild(getChild(i)); // addChild() calls deepCopy()
        }
        return res;
    }

    private String text;
    private ArrayList<Expr> children;
}
