package util;

import java.util.LinkedHashMap;

/**
 * generate unique ID
 *
 */

public class GenID {

    public static String nextIdFor(String category) {
        if (!tab.containsKey(category)) {
            tab.put(category, 0);
        }
        tab.put(category, tab.get(category).intValue() + 1);
        return category + tab.get(category).toString();
    }
    
    public static void reset(String category) {
        if (tab.containsKey(category)) {
            tab.put(category, 0);
        }
    }

    private static LinkedHashMap<String, Integer> tab = new LinkedHashMap<String, Integer>();
}
