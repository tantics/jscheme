(define-syntax for
  (syntax-rules (in)
    ((for element in lst body ...)
     (map (lambda (element)
            body ...)
          lst))))

; make 'map' point to other things
(define foo 
  (lambda ()
    (define map 1)
    (for i in (quote (macro element body lst)) (display i) (newline))))


(foo)

