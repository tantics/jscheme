(define-syntax swap
  (syntax-rules ()
    ((swap a b)
      (begin
        (define temp a)
        (set! a b)
        (set! b temp)))))

(define x 1)
(define temp 2)
(display (cons x temp)) (newline)
(swap x temp)
(display (cons x temp)) (newline)
