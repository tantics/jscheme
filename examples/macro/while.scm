(define-syntax while
  (syntax-rules ()
    ((while condition body ...)
     (begin  
       (define loop 
         (lambda ()
           (if condition
               (begin
                 body ...
                 (loop))
               #f)))
       (loop)))))

(define x 0)
(while (< x 10)
       (set! x (+ x 1))
       (display x)
       (newline))
