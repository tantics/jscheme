(define square
  (lambda (x)
    (* x x)))

(define even?
  (lambda (n)
    (= (remainder n 2) 0)))

(define fast-expt
  (lambda (b n)
    (if (= n 0)
        1
        (if (even? n)
            (square (fast-expt b (/ n 2)))
            (* b (fast-expt b (- n 1)))))))

(fast-expt 2 10)
