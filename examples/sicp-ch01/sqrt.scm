(define sqrt
    (lambda (x)
        (define average
          (lambda (a b)
            (/ (+ a b) 2)))
        (define square
          (lambda (a)
            (* a a)))
        (define improve
          (lambda (guess)
            (average guess (/ x guess))))
        (define good-enough?
          (lambda (guess)
            (< (abs (- (square guess) x)) 0.0001)))
        (define sqrt-iter 
          (lambda (guess)
            (if (good-enough? guess)
                guess
                (sqrt-iter (improve guess)))))
        (sqrt-iter 1)))

(sqrt 2)
