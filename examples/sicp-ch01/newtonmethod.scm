(define dx 0.00001)

(define tolerance 0.00001)

(define square
  (lambda (x) (* x x)))


(define fixed-point
  (lambda (f first-guess)
    (define close-enough?
      (lambda (v1 v2)
        (< (abs (- v1 v2)) tolerance)))
    (define try
      (lambda (guess)
        (define next (f guess))
        (if (close-enough? guess next)
            next
            (try next))))
    (try first-guess)))

(define deriv
  (lambda (g)
    (lambda (x)
      (/ (- (g (+ x dx)) (g x))
         dx))))

(define newton-transform
  (lambda (g)
    (lambda (x)
      (- x(/ (g x) ((deriv g) x))))))

(define newtons-method 
  (lambda (g guess)
    (fixed-point (newton-transform g) guess)))


(define sqrt
  (lambda (x)
    (newtons-method (lambda (y) (- (square y) x)) 1.0)))

(sqrt 2)
