(define sum
  (lambda (term a next b)
    (if (> a b)
        0
        (+ (term a)
           (sum term (next a) next b)))))

(define integral
  (lambda (f a b dx)
    (* (sum f (+ a (/ dx 2.0)) (lambda (x) (+ x dx)) b) dx)))

(define cube
  (lambda (x)
    (* x x x)))

(integral cube 0 1 0.001)
