(define tolerance 0.00001)

(define fixed-point
  (lambda (f first-guess)
    (define close-enough?
      (lambda (v1 v2)
        (< (abs (- v1 v2)) tolerance)))
    (define try
      (lambda (guess)
        (define next (f guess))
        (if (close-enough? guess next)
            next
            (try next))))
    (try first-guess)))

(fixed-point (lambda (y) (+ (sin y) (cos y))) 1.0)
