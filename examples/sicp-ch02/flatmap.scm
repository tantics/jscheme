(define append
  (lambda (list1 list2)
    (if (null? list1)
        list2
        (cons (car list1) (append (cdr list1) list2)))))

(define map
  (lambda (proc items)
    (if (null? items)
        nil
        (cons (proc (car items))
              (map proc (cdr items))))))

(define accumulate
  (lambda (op initial sequence)
    (if (null? sequence)
        initial
        (op (car sequence)
            (accumulate op initial (cdr sequence))))))

(define flatmap
  (lambda (proc seq)
    (accumulate append nil (map proc seq))))

(flatmap (lambda (j) (list j j)) (list 1 2 3 4 5))
