(define append
  (lambda (list1 list2)
    (if (null? list1)
        list2
        (cons (car list1) (append (cdr list1) list2)))))

(define make-leaf
  (lambda (symbol weight)
    (list (quote leaf) symbol weight)))

(define leaf?
  (lambda (object)
    (eqv? (car object) (quote leaf))))

(define symbol-leaf
  (lambda (x)
    (car (cdr x))))

(define weight-leaf
  (lambda (x)
    (car (cdr (cdr x)))))

(define make-code-tree
  (lambda (left right)
    (list left
          right
          (append (symbols left) (symbols right))
          (+ (weight left) (weight right)))))

(define left-branch
  (lambda (tree)
    (car tree)))

(define right-branch
  (lambda (tree)
    (car (cdr tree))))

(define symbols
  (lambda (tree)
    (if (leaf? tree)
        (list (symbol-leaf tree))
        (car (cdr (cdr tree))))))

(define weight
  (lambda (tree)
    (if (leaf? tree)
        (weight-leaf tree)
        (car (cdr (cdr (cdr tree)))))))

(define decode
  (lambda (bits tree)
    (define decode-1
      (lambda (bits current-branch)
        (if (null? bits)
            nil
            ((lambda ()
              (define next-branch (choose-branch (car bits) current-branch))
              (if (leaf? next-branch)
                  (cons (symbol-leaf next-branch)
                        (decode-1 (cdr bits) tree))
                  (decode-1 (cdr bits) next-branch)))))))
  (decode-1 bits tree)))

(define choose-branch
  (lambda (bit branch)
    (if (= bit 0)
        (left-branch branch)
        (right-branch branch))))
              
(define sample-tree
  (make-code-tree (make-leaf (quote a) 4)
                  (make-code-tree
                    (make-leaf (quote b) 2)
                    (make-code-tree (make-leaf (quote d) 1)
                                    (make-leaf (quote c) 1)))))

(define sample-message (quote (0 1 1 0 0 1 0 1 0 1 1 1 0)))

(decode sample-message sample-tree)
