(define map
  (lambda (proc items)
    (if (null? items)
        nil
        (cons (proc (car items))
              (map proc (cdr items))))))

(map abs (list -10 2.5 -11.6 17))
