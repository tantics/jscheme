(define memq
  (lambda (item x)
    (if (null? x)
        #f
        (if (eqv? item (car x))
            x
            (memq item (cdr x))))))

(memq (quote apple) (quote (x (apple sauce) y apple pear)))
