(define filter
  (lambda (predicate sequence)
    (if (null? sequence)
        nil
        (if (predicate (car sequence))
            (cons (car sequence)
                  (filter predicate (cdr sequence)))
            (filter predicate (cdr sequence))))))

(define odd?
  (lambda (x)
    (not (= (remainder x 2) 0))))

(filter odd? (list 1 2 3 4 5))
