(define front-ptr
  (lambda (queue)
    (car queue)))

(define rear-ptr
  (lambda (queue)
    (cdr queue)))

(define set-front-ptr!
  (lambda (queue item)
    (set-car! queue item)))

(define set-rear-ptr! 
  (lambda (queue item)
    (set-cdr! queue item)))

(define empty-queue?
  (lambda (queue)
    (null? (front-ptr queue))))

(define make-queue
  (cons nil nil))

(define front-queue
  (lambda (queue)
    (if (empty-queue? queue)
        nil
        (car (front-ptr queue)))))

(define insert-queue!
  (lambda (queue item)
    (define new-pair (cons item nil))
    (if (empty-queue? queue)
        (begin
          (set-front-ptr! queue new-pair)
          (set-rear-ptr! queue new-pair)
          queue)
        (begin
          (set-cdr! (rear-ptr queue) new-pair)
          (set-rear-ptr! queue new-pair)
          queue))))

(define delete-queue!
  (lambda (queue)
    (if (empty-queue? queue)
        nil
        (begin
          (set-front-ptr! queue (cdr (front-ptr queue)))
          queue))))

(define q1 make-queue)
(insert-queue! q1 (quote a))
(display q1) (newline)
(insert-queue! q1 (quote b))
(display q1) (newline)
(delete-queue! q1)
(display q1) (newline)
(insert-queue! q1 (quote c))
(display q1) (newline)
(delete-queue! q1)
(display q1) (newline)
(insert-queue! q1 (quote d))
(display q1) (newline)
